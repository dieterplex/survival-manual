## Chapter 2: Survival Medicine

> Isolated persons require knowledge of many different survival skills to return to friendly forces. This chapter will discuss the psychology of survival, treatment for shock, and survival medicine practices.

### SURVIVAL MEDICINE VERSUS TRADITIONAL MEDICINE

2-1. Excluding the enemy, medical related problems arising from combat and isolation pose the greatest
threat to isolated personnel. Isolated personnel must understand the vast differences and transition of medical
care that take place following combat, isolating events, and implementation of the isolated person's ISG/EPA.
Isolated personnel must be able to perform fundamental survival medicine techniques throughout the duration
of their isolation including during evasion, detention or captivity, and recovery. During combat operations,
injured Soldiers follow Tactical Combat Casualty Care (TCCC) guidelines and protocols focused on the care
of casualties in a combat or tactical environment at the point-of-injury. The TCCC program of instruction
prepares Soldiers to provide self-aid or buddy-aid in the absence of a medical provider. Four transitions of
medical care occur on the battlefield that includes isolation producing events.
* Perform Care under Fire (CUF) at the point-of-injury on the battlefield. The Soldier is typically
equipped with the Improved First Aid Kit (IFAK), Combat Pill Kit, and Eye Shield. The Soldier
returns fire and takes cover while focused on the treatment of massive hemorrhage.
* Perform Tactical Field Care when no longer under hostile fire. The Soldier is equipped with the
Individual First Aid Kit (IFAK), Combat Pill Kit, and Eye Shield. The Soldier is subject to a
reduced level of hazard from hostile fire, more time is available to provide care based on the
tactical situation, a security perimeter is established and the casualty evaluated for altered mental
status. Follow the MARCH algorithm as a guide to the sequence of treatment priorities in caring
for combat casualties:
  - **M**assive hemorrhage — control life threatening bleeding.
  - **A**irway — establish and maintain a patent airway.
  - **R**espiration — decompress suspected tension pneumothorax, seal open chest wounds, and
support ventilation/oxygenation as required.
  - **C**irculation — establish intravenous (IV) access and administer fluids as required to treat
shock.
  - **H**ead injury/Hypothermia — prevent/treat hypotension and hypoxia to prevent worsening of
traumatic brain injury and prevent/treat hypothermia.
* Perform medical care at the point-of-injury throughout isolation. Typically, the Soldier's IFAK
and supporting medical supplies are already expended; therefore, Survival medicine takes over at
this point. Survival medicine recognizes the fundamentals of the MARCH algorithm and provides
a flexible approach that prioritizes actions relevant to physical, psychological, and environmental
considerations that are continually assessed and prioritized for performance by isolated personnel.

2-2. Survival medicine requires isolated personnel to—
* Understand the fundamentals of trauma first aid (MARCH) and survival medicine.
* Assess and prioritize applicable fundamentals from trauma first aid and survival medicine in a
resource-constrained environment.
* Execute those fundamentals under sub-optimal healing conditions.
* Continually assess the isolated person's mental state and enables appropriate adjustments.
* Understand that medical personnel and facilities are rarely available.

2-3. Survival medicine employs four techniques to facilitate isolated personnel performance of survival
proficiencies. These techniques include—
* **Prevention**. The common sense act of proactive prevention or hindrance of an action that could
lead to a requirement to perform trauma first aid or survival medicine. For example, during evasion
movement, the Soldier looks for alternate crossing points across a river instead of swimming
across to decrease the chance of getting hypothermia and having to dry their clothing.
* **Recognition**. The act of actively recognizing—
  - Symptoms (urine color and relation to dehydration).
  - Capabilities (identify local medicinal plants such as cattails, willow during movement).
  - Identity (poisonous snakes or insects) by the isolated person.
* **Mitigation**. The actions taken to immediately reduce the severity and or pain associated with a
survival related injury or illness (application of a tourniquet to stop severe, uncontrolled bleeding
that could cause loss of life)
* **Treatment**. The actions taken to manage and care for the isolated person with a disease or disorder
and restore their health. The ability of the isolated person or personnel to plan, prepare, execute
and assess appropriate actions to meet their survival needs significantly increases their overall
chances of survival, increases morale, and aids in their ability to perform the survival proficiencies
and eventual return to friendly forces.

*** PERSONAL HYGIENE AND SANITATION

2-4. Isolated persons can avoid many different kinds of illnesses and infections by practicing good
sanitation and hygiene. Maintaining a clean body and living area will prevent the spread of germs and bacteria
whether alone or in a group. It will also allow isolated persons to stay organized and protect items from
animal contact.

2-5. Application of the following guidelines regarding personal health and hygiene will enable isolated
personnel to safeguard personal health and the health of others while detained or captive.
* Stay clean (daily regimen).
  - Minimize infection by washing. (Use white ashes, sand, or loamy soil as soap substitutes.)
  - Comb and clean debris from hair.
  - Wash your hands thoroughly after handling any material that is likely to carry germs, after
urinating or defecating, after caring for the sick, and before handling any food, food utensils,
or drinking water.
* Cleanse mouth and brush teeth.
  - Use hardwood twig as toothbrush (fray it by chewing on one end then use as brush).
  - Use a single strand of an inner core string from parachute cord for dental floss.
  - Use a clean finger to stimulate gum tissues by rubbing.
  - Gargle with salt water to help prevent sore throat and aid in cleaning teeth and gums.
* Clean and protect feet.
  - Change and wash socks
  - Wash, dry, and massage.
  - Check frequently for blisters and red areas.
  - Use adhesive tape/mole skin to prevent damage.
* Keep your clothing and bedding as clean as possible to reduce the chances of skin infection or
parasitic infestation.
* Exercise daily.
* Prevent and control parasites.
  - Check body for lice, fleas, ticks.
  - Check body regularly.
  - Pick off insects and eggs (DO NOT crush).
  - Use commercial and expedient (ex. rub cedar boughs on your body) repellants.
  - Use smoke to fumigate clothing and equipment.
* Avoid illness.
  - Purify all water obtained from natural sources by using iodine tablets, bleach, or boiling for
5 minutes.
  - Locate latrines 200 feet from water and away from shelter.
  - Clean all eating utensils after each meal.
  - Prevent insect bites by using repellent, netting, and clothing.
  - Eat varied diet.
  - Try to get 7-8 hours' sleep per day.

2-6. Survival medicine capabilities in the Survival Kit. Prior to operations, personnel assess the survival kit
to identify its survival medicine components. It is recommended that the Soldier rehearse appropriate survival
medicine techniques using similar materials as those found in the actual survival kit. The survival medicine
capabilities that should be considered for inclusion in the survival kit includes these items:
Water purification tablets.
* Small tube of antibiotic ointment.
* 1 oz. bottle of 2% tincture of iodine.
* Small tube of Crazy Glue/Super Glue.
* Small tube/bottle of Betadine.
* Emergency blanket.
* Floss card (dental floss).
* Small roll of Duct tape with peel-away backing.
* Heavy duty canvas sewing needle.
* Assorted dressings.
* Assorted bandages.
* Combat Gauze.
* SOFT-T or combat application tourniquet (CAT) or Israeli bandage.
* Small package of prescription medications, contacts.
* Gear that accomplishes more than one task (ex. Use a large bandana as a compress, sling, bandage,
and eye patch).
