# Survival Manual

> This section will give you some information regarding this application, but **if you are in a survival situation right now** - please **skip this blurb** and jump directly to the section that contains the information you need.

## Foreword
This book is based on the original US Army [FM 3-05.70 Field Manual](https://fas.org/irp/doddir/army/fm3-05-70.pdf) which was kindly edited and shortened to [Ligi's Survival Manual](https://github.com/ligi/SurvivalManual) and is available in a [wiki](https://github.com/ligi/SurvivalManual/wiki). However, it was ultimately thought to be consumed by a specific app, and is not set up for internationalization. And by now, the 2018 revision of the field manual is out and shouts for updating of the content. So we will start with the original content by Ligi and gradually replace this with the updated content. The current online edition of the book can always be found [here](https://sspaeth.gitlab.io/survival-manual/), and a epub version of the book can be downloaded and read offline from here.

Last but not least, the original field manual is in the public domain, all content in this book is licensed under the CC0 license (so you can basically do with it whatever you want).

If you want to contribute to the book, provide illustrations, help to translate it (not yet!) or suggest improvements, feel free to do so. Other things I could think of, are region-specific adaptations to e.g. plants and animals in Europe vs Asia, etc...

## About the Book

This app is intended to guide you in rough times, but it can also be used to have fun outdoors and to learn things that are useful to know in case of an emergency. The fun parts can also be used to train important skills for potential survival situations. Remember, reading is only one factor - practice and understanding are also needed to increase your skillset and improve your chances of survival.

If you have a question, please check out [the FAQs](FAQ) - it is possible your question is already answered there.

Thanks to everyone who made this app possible! You can find some of them [in the credits](Credits).

## Original Introduction

ATP 3-50.21, Survival, sets forth the doctrine pertaining to survival in an isolated situation. The ATP
discusses the tenets of survival and the methods Soldiers, DA civilians, and DA contractors can use when
surviving individually or in a group. It aligns and nests with FM 3-50’s discussion of personnel recovery.
The personnel recovery mission includes preparing Army personnel in danger of isolation while participating
in any activity or mission sponsored by the United States. Isolation refers to persons separated from their unit
or in a situation where they must survive, evade, resist, or escape. ATP 3-50.21 contains nine chapters and
two appendixes.
The following is a brief description of each chapter and appendix:
[Chapter 1](./01_PersonalRecovery.md) provides an overview of personnel recovery and discusses survival proficiencies.
[Chapter 2](./02_Medicine) discusses survival medicine applications.
Chapter 3 covers water collection methods.
Chapter 4 discusses food collection and preparation methods.
Chapter 5 focuses on fire craft for survival.
Chapter 6 covers constructing shelters in the field and clothing.
Chapter 7 discusses land navigation methods.
Chapter 8 covers survival, evasion and recovery equipment.
Appendix A discusses ropes and knots useful for survival applications.
The U.S. Army Personnel Recovery Proponent made a decision to divide the revision of FM 3-05.70,
Survival from one large volume into separate ATPs for easier access and reading. ATP 3-50.21 is the second
in a series of ATPs for Army personnel recovery. The series also includes the following:
z ATP 3-50.20, Survival, Evasion, Resistance and Escape (SERE) Preparation and Planning,
discusses the preparation and training required to conduct SERE operations in a theater.
z ATP 3-50.22, Evasion, discusses evasion techniques used to evade enemy forces and prepare for
recovery from friendly forces or by self-recovery.
